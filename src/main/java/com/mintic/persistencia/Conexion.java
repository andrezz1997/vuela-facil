/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mintic.persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author angelica
 */
public class Conexion {
    
    // Atributos
    private String url="jdbc:mysql://10.42.0.2:3306/";
    private String user="root";
    private String password="red local";
        
    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    private void setUrl(String database) {
        this.url = "jdbc:mysql://10.42.0.2:3306/" + database;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    
    // Constructor
    public Conexion(String database){
        // jdbc:mysql protocolo de cnexión a la DB
        // luego viene el dominio - localhost -> :3306 es el puerto
        this.setUrl(database);
    }
    
    // Métodos
    public Connection iniciarConexion() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection=DriverManager.getConnection(getUrl(),getUser(),getPassword());
        System.out.println("Connection is successful to the database "+getUrl());
        return connection;
    }
}  
