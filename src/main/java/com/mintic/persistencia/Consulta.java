/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mintic.persistencia;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author andres
 */
public class Consulta {
    public static void request(String sqlRequest, String database) throws SQLException, ClassNotFoundException{
        Conexion conexion = new Conexion(database);
        Connection conex = conexion.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sqlRequest);
    }
}
