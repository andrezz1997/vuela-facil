/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mintic.rutas;

/**
 *
 * @author angelica
 */
public class Vuelo {
    
    //Atributos
    private int id;
    private int id_avion; 
    private String ubicacion_actual;
    private String ubicacion_siguiente;
    
    // Constructors
    public Vuelo(int id, int id_avion, String ubicacion_actual, String ubicacion_sguiente){
       this.id = id;
       this.id_avion = id_avion;
       this.ubicacion_actual = ubicacion_actual;
       this.ubicacion_siguiente = ubicacion_siguiente;
    }

    // Getters & Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdAvion() {
        return this.id_avion;
    }

    public void setIdAvion(int id_avion) {
        this.id_avion = id_avion;
    }

    public String getUbicacion_actual() {
        return ubicacion_actual;
    }

    public void setUbicacion_actual(String ubicacion_actual) {
        this.ubicacion_actual = ubicacion_actual;
    }

    public String getUbicacion_siguiente() {
        return ubicacion_siguiente;
    }

    public void setUbicacion_siguiente(String ubicacion_siguiente) {
        this.ubicacion_siguiente = ubicacion_siguiente;
    }

    @Override
    public String toString() {
        return "Vuelos{" + "id=" + id + ", id_avion=" + id_avion +
                ", ubicacion_actual=" + ubicacion_actual + ", ubicacion_siguiente=" + ubicacion_siguiente + '}';
    } 
}
