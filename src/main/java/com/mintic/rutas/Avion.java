/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mintic.rutas;

/**
 *
 * @author angelica
 */
public class Avion {
    
    //Atributos
    private int id;
    private String matricula;
    private String modelo;
    
    // Constructor
    public Avion(int id, String matricula, String modelo){
        this.id = id;
        this.matricula = matricula;
        this.modelo = modelo;
    }

    // Getters & Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

   public Avion(int id, String modelo){
       this.setId(id);
       this.setModelo(modelo);
   }

    @Override
    public String toString() {
        return "Vuelos{" + "id=" + id + ", modelo=" + modelo + '}';
    }
}
