/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mintic.logica;

import java.util.Scanner;
import java.sql.SQLException;
import com.mintic.persistencia.*;
import com.mintic.usuarios.*;

/**
 *
 * @author angelica
 */
public class Main {
    
    static String title =
"-------------------------------------------------------\n" + 
"__     __          _         _____          _ _ \n" +
"\\ \\   / /   _  ___| | __ _  |  ___|_ _  ___(_) |\n" +
" \\ \\ / / | | |/ _ \\ |/ _` | | |_ / _` |/ __| | |\n" +
"  \\ V /| |_| |  __/ | (_| | |  _| (_| | (__| | |\n" +
"   \\_/  \\__,_|\\___|_|\\__,_| |_|  \\__,_|\\___|_|_|\n" +
"-------------------------------------------------------\n";
    static int bandera = 0;
    static Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        while (bandera == 0){
            System.out.print("\033[H\033[2J");
            System.out.flush();
            System.out.print(title);
            System.out.print("¿Qué desea hacer? \n");
            System.out.print("1. Registrarme \n");
            System.out.print("2. Iniciar sección \n");
            bandera = sc.nextInt();
        }
        switch (bandera) {
            case 1:
                sc = new Scanner(System.in);
                registrarse();
            case 2:
                sc = new Scanner(System.in);
                iniciarSeccion();
            default:
                sc = new Scanner(System.in);
                break;
        }
    }

    private static void registrarse() {
        System.out.printf("Ingrese su nombre: \n");
        String nombre = sc.nextLine();
        System.out.printf("Ingrese su apellido: \n");
        String apellido = sc.nextLine();
        System.out.printf("Ingrese su email: \n");
        String email = sc.nextLine();
        System.out.printf("Ingrese su contraseña: \n");
        String passwd = sc.nextLine();
        System.out.printf("¿Es usted administrador? S/N \n");
        if (sc.nextLine() == "N"){
            Usuario usuario = new Cliente(nombre,apellido,email,passwd,"cliente");
            usuario.registrarse();
        } else {
            Usuario usuario = new Administrador(nombre,apellido,email,passwd,"administrador");
            usuario.registrarse();
        }       
    }
    
    private static void iniciarSeccion() {
        System.out.printf("Ingrese su email: \n");
        String email = sc.nextLine();
        System.out.printf("Ingrese su contraseña: \n");
        String contraseña = sc.nextLine();
        System.out.print("¡Bienvenido!\n");
        
    }

    
}
