/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mintic.usuarios;

import com.mintic.persistencia.Consulta;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author angelica
 */
public class Administrador extends Usuario {
    public Administrador(String nombre, String apellido, String email, String passwd, String area){
        super(nombre,apellido,email,passwd,area);
    }

    // CRUD
    @Override
    public void registrarse() {
        try {
            String sqlRequest = "INSERT INTO admins (nombre,apellido,email,passwd) VALUES (" + this.getNombre() + "," + this.getApellido() + "," + this.getEmail() + "," + this.getPasswd() + ");";
            Consulta.request(sqlRequest, "usuarios");
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void actualizar(String nombre, String apellido, String passwd) {
        try {
            String sqlRequest = "UPDATE admins SET nombre,apellido,passwd WHERE email=" + this.getEmail() + ");";
            Consulta.request(sqlRequest, "usuarios");
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void eliminar(int id) {
        try {
            String sqlRequest = "DELETE FROM admins WHERE email=" + this.getEmail() + ");";
            Consulta.request(sqlRequest, "usuarios");
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
