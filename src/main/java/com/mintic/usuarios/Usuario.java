/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mintic.usuarios;

import com.mintic.persistencia.Conexion;
import com.mintic.persistencia.Consulta;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author angelica
 */
public abstract class Usuario {
    // Atributos
    private int id;
    private String nombre;
    private String apellido;
    private String email;
    private String passwd;
    private String area;
    
    // Constructor
    public Usuario(String nombre, String apellido, String email, String passwd, String area){
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.passwd = passwd;
        this.area = area;
    }
    
    //Getters & Setterss
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }
    
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
    
    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", nombre=" + nombre + ", apellido=" + 
                apellido + ", email=" + email +", password=" + passwd + ", area=" + getArea() + "}";
    }
    
    // CRUD
    
    public abstract void registrarse();
    
    public static void leer(String email){
        try {
            Consulta.request("SELECT nombre,apellido FROM clientes WHERE email = " + email +";", "usuarios");
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public abstract void actualizar(String nombre, String apellido, String passwd);
    
    public abstract void eliminar(int id);
}